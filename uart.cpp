#include "header.h"
#include "uart.h"

void uart_init() {
	Serial.begin(9600);
}

char uart_busy() {
	return !Serial.availableForWrite();
}

void uart_putc(char c) {
	Serial.write(c);
}

unsigned char uart_getc() {
	return Serial.read();
}

char uart_dataRdy() {
	return Serial.available();
}
