#include "header.h"
#include "commands.h"
#include "shell.h"
#include "command_list.h"

/*
Each command must be pre-declared.
Also they should be added into array of command's structs.
Each command should be declared as "void command_name(unsigned char *parsedCommand[])"
*/

//Pre-declaration of commands
extern Command com_on;
extern Command com_off;
static unsigned char com_help(unsigned char* parsedCommand[]);


//Array of command's structures contains name of command that will be used in terminal for executing command and reference to function that contains command.
Command commands[] = {
	com_on,
	com_off,
	{"help", com_help}, 
	{"", 0}
};

static unsigned char com_help(unsigned char* parsedCommand[]) {
	char n[] = "\n";
	int i;
	for (i = 0; commands[i].command != 0; i++) {
		out_pushString(n);
		out_pushString(commands[i].name);		
	}
 return 1;
}


