#include "command.h"
#include "header.h"
#include "com_on.h"
#include "shell.h"

//Example of command
static unsigned char _com_on(unsigned char* parsedCommand[]) {	
	char msg[30] = "\nSyntax error\n";
	char success[30] = "\nSuccess\n";
	if (parsedCommand[3] == 0 && parsedCommand[1] != 0)	{
		unsigned char bit = atoi((parsedCommand[1]));
		digitalWrite(bit, HIGH);
		out_pushString(success);
    return 1;
	}	else {
		out_pushString(msg);
    return 0;
	}
}

Command com_on = {"on", _com_on};
