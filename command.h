#ifndef __COMMAND_H
#define __COMMAND_H

typedef struct {
	char name[10];
	unsigned char (*command)(unsigned char*[]);
} Command;

#endif