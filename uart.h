#ifndef __UART_H
#define __UART_H

void uart_init();

char uart_busy();

void uart_putc(char c);

unsigned char uart_getc();

char uart_dataRdy();

#endif